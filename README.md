# 112_googletest_small

Google Test; tested in windows and linux 

# Very Small Example of C++ Unit Testing with GoogleTest 

"Software Design" course of M.EEC, in FEUP, Portugal
Armando Sousa    [email](mailto:asousa@fe.up.pt)
Based on
  https://www.eriksmistad.no/getting-started-with-google-test-on-ubuntu/  
  https://github.com/smistad/GTest



## License
[GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)
In short, free software: Useful, transparent, no warranty


## Very small program with one function and its test

## Instalation and testing

Tested on Windows and Linux

Naturally, needs GoogleTest libraries

On windows https://packages.msys2.org/base :
````
pacman -S mingw-w64-x86_64-gtest
````

On linux, follow https://www.eriksmistad.no/getting-started-with-google-test-on-ubuntu/




````
cmake CMakeLists.txt
make
./runTests
````


### Repo for inspiration
https://github.com/smistad/GTest



### References (additional information)

 * Google Test Wiki - http://code.google.com/p/googletest/wiki/Documentation

 * IBM A quick introduction to the Google C++ Testing Framework - http://www.ibm.com/developerworks/aix/library/au-googletestingframework.html

 * Google Test Installation Guide for C++ for Visual Studio Code - https://medium.com/swlh/google-test-installation-guide-for-c-in-windows-for-visual-studio-code-2b2e66352456

 * Google Test Release Git - https://github.com/google/googletest/tree/release-1.10.0

 * MSYS (compiler and package manager for windows, in order to achieve cross platform) 
    * https://www.msys2.org/wiki/MSYS2-installation/
    * https://packages.msys2.org/package/
    * https://packages.msys2.org/package/mingw-w64-x86_64-gtest
    * https://packages.msys2.org/package/mingw-w64-x86_64-gtest?repo=mingw64

To install Google Test: ` pacman -S mingw-w64-x86_64-gtest `




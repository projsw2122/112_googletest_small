//https://www.eriksmistad.no/getting-started-with-google-test-on-ubuntu/

// whattotest.cpp

#include <math.h>
 
double squareRoot(const double a) {

    // if (a>35.0) return 35.0; // Provoke an error !!!

    double b = sqrt(a);
    if(b != b) { // nan check
        return -1.0;
    }else{
        return sqrt(a);
    }
}